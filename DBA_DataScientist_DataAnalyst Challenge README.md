------------------------------- Challenge instructions -------------------------------

1) Running the code:
open the Theta.py in any python IdE and run the file ( data files should be present in same folder  and data files should have a column named 'Id')


2) Files :
attached are Theta.py , a small dataset of 20 records( Data_1.csv , Data_1.tsv  ), a larger dataset of 22000 records (Data_2.csv , Data_2,tsv )


3)About the Project:
The script is written in python, it accepts two filenames from the user  "example ( Data_1.csv , Data_1,tsv )" ,
One .csv file and one .tsv file. ( Note The files should be present in the same folder as the python file.)


4)Working : Code matches the records in both files using the 'Id' column and creates a new folder 'Output + name of first file + name of second file, and a file named as Data,tsv to write the consistent records.
The records which are found inconsistent are written into a file named error.txt present in a new folder nmaed Error + name of file1 + name of file2

5) Code checks for 2 types of inconsistencies : a records in file 1 but not on file two and vice versa , in these cases the code reports the record in the
error.txt file created in error folder , and mentions that the records is missing certain columns
  Next type of inconsistency is Data inconsistency :
    a record with same row id present in two files has conflicting entries for the same column, the code reports the record in the error file.
    
7) Model design : the code uses hashmaps ( default dictionaries ) and runs in liner time ( ex : files with 90000 entries are checked in less than 3 secs )

6) Datasets : I have provided 2 data sets namely Data_1 and Data_2 both datasets have one .csv and one ,tsv files. Data_1 is a samller dataset with 20 entries 
and data_2 is a large dataset with 22000 entries.

      




